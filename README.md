# Community CiviCRM

# Name
Repo for testing and installating CiviCRM

# Description
This is a base drupal and civicrm installation. It is used for deployments of
CiviCRM to the cloud and production VPS projects. It was installed locally
using composer and the files copied.

# Installation
The intent is that this repo can be cloned on to a host, settings files
templated into place, and a database import completed. This should result in a
minimally functional CiviCRM installation.

## Database
Before the database import can be completed, the following setup must be done:

```
SET GLOBAL TRANSACTION ISOLATION LEVEL READ COMMITTED;
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

CREATE DATABASE drupal CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE USER civi_admin@localhost IDENTIFIED BY '<PASSWORD_GOES_HERE>';
CREATE USER civi_admin@127.0.0.1 IDENTIFIED BY '<PASSWORD_GOES_HERE>';

GRANT SUPER ON *.* TO 'civi_admin'@'localhost';
GRANT SUPER ON *.* TO 'civi_admin'@'127.0.0.1';

GRANT ALL ON drupal.* TO 'civi_admin'@'localhost';
GRANT ALL ON drupal.* TO 'civi_admin'@'127.0.0.1';

flush privileges;
```

Additionally, these mariadb settings should be added to the mariadb config
file:

```
log_bin_trust_function_creators = 1
transaction_isolation = "READ-COMMITTED"
```

## Drupal / CiviCRM
The drupal and CiviCRM install was completed using the following steps:

```
cd /project/root

composer create-project drupal/recommended-project:9.5.11 civicrm

cd civicrm/

composer config extra.enable-patching true
composer config minimum-stability dev
composer require civicrm/civicrm-{core,packages,drupal-8}:~5.64.2

# When prompted, choose always:
# Allow these packages to compile? ([y]es, [a]lways, [n]o, [l]ist, [h]elp) a

composer require drush/drush
composer require civicrm/cv

cp web/sites/default/default.settings.php \
    web/sites/default/settings.php

./vendor/bin/drush site:install \
    --db-su=civi_admin \
    --db-su-pw=<PASSWORD_GOES_HERE> \
    --db-url=mysql://civi_admin:<PASSWORD_GOES_HERE>@localhost:3306/drupal \
    --account-name=civi_admin \
    --account-mail=fr-tech@wikimedia.org \
    --account-pass=<PASSWORD_GOES_HERE> \
    --site-mail=fr-tech@wikimedia.org \
    --locale=en \
    --site-name=community-crm \
    --yes

chmod u+w ./web/sites/default/

./vendor/bin/cv core:install \
    --cms-base-url="https://org.example.civitest" \
    --db=mysql://civi_admin:<PASSWORD_GOES_HERE>@localhost:3306/drupal \
    -m loadGenerated=1
```

The site specific configuration files are excluded from git and are templated
using puppet.

## Post install steps

You may need to do some post install steps for it to function correctly. We
have these codified in puppet.

### Fix permissions on files and directories

sudo chgrp -R www-data ./web/

### But we don't want the settings file or enclosing directory writable

chmod -w /var/www/org.example.civitest/civicrm/web/sites/default/civicrm.settings.php
chmod g-w /var/www/org.example.civitest/civicrm/web/sites/default

# Updates

To update the components for this repo you need to have it installed and
functional on a testing machine. That machine also has to have composer
installed. With that, you can run the following:

```
cd /project/root
composer update
composer update "drupal/core-*" --with-all-dependencies
```

You can then rsync the files needed to the repo directory excluding git files
and additional files to keep them from getting added or deleted.

```
rsync -avh --cvs-exclude --exclude=README.md --exclude=.gitignore --exclude=.gitlab-ci.yml --delete /project/root/org.example.civitest/civicrm/ /repo/directory/
```

Once that is complete, review the changes with git, commit, and merge them.
You will then be able to use the deploy mechanism to update your files on
disk.
